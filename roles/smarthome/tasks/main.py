from pyplays import Tasks

with Tasks() as t:
    t.yum_repository(
        name="OpenHAB",
        description="OpenHAB Stable",
        baseurl="https://openhab.jfrog.io/artifactory/openhab-linuxpkg-rpm/stable",
        gpgcheck=True,
        gpgkey="https://openhab.jfrog.io/artifactory/api/gpg/key/public")
    t.package(name=["openhab", "java-17-openjdk", "nftables"], state="latest")

    # RXTX has this path hardcoded in its source code!
    # Hopefully nobody else is using that directory
    t.file(path="/var/lock", state="directory", mode=0o777)
    t.copy(dest="/etc/tmpfiles.d/10-lock.conf",
        content="d /run/lock 0777 root root -")
    t.service(name="systemd-tmpfiles-setup", state="started")

    t.file(path="/ssd_data/openhab", state="directory", mode=0o700)
    t.file(path="/ssd_data/openhab", owner="openhab", group="openhab", recurse=True) # For existing data
    t.file(path="/ssd_data/openhab/backups", state="directory", owner="openhab")
    t.file(path="/ssd_data/openhab/secrets", state="directory", owner="openhab")
    t.file(path="/ssd_data/openhab/jsondb", state="directory", owner="openhab")
    t.file(
        src="/ssd_data/openhab/secrets",
        dest="/var/lib/openhab/secrets",
        state="link",
        force=True)
    t.file(
        src="/ssd_data/openhab/jsondb",
        dest="/var/lib/openhab/jsondb",
        state="link",
        force=True)
    t.copy(dest="/etc/default/openhab",
        content="""
OPENHAB_BACKUPS=/ssd_data/openhab/backups
""")

    # Only allow caddy-rp to connect to the unauthenticated OpenHAB server
    t.copy(dest="/etc/sysconfig/nftables.conf",
        content="""
flush ruleset

table inet filter {
        chain input {
                type filter hook input priority 0;
                tcp dport { 8080, 8443, 5007 } \
                ip saddr != {{ hostvars['caddy-rp'].ip }} \
                reject;
        }
}""")
    t.service(name="nftables", state="reloaded", enabled=True)

    t.service(name="openhab", state="restarted", enabled=True)

