#!/usr/bin/env sh
DEST=/shareddata/tls_certs/
find /home/caddy -name "*.crt" -or -name "*.key" \
	| xargs ls -1rt \
	| while read file; do
		cp $file "$DEST"
	done
chmod g+r "$DEST"/*
