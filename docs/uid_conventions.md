# UIDs

0-999 -- System users
1000-1999 -- Non-data users
2000-2999 -- Data users
4000 -- Shared container data
5000 -- Auxiliary LDAP users

# GIDs
0-999 -- System	groups
1000-1999 -- Non-data groups
2000-2999 -- Data users' groups
3000-3999 -- Data groups
4000 -- Shared container data

The system and non-data users/groups are stored locally.

Data users and groups are stored in the LDAP database and passed through to
containers via idmap.
