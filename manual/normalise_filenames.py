#!/usr/bin/env python3
import sys
import pathlib
import unicodedata as ud

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: {} <directory>".format(sys.argv[0]), file=sys.stderr)
        sys.exit(1)
    
    directory = pathlib.Path(sys.argv[1])
    for f in directory.glob("**/*"):
        norm_name = ud.normalize("NFC", f.name)
        #f.rename(norm_name)
        if norm_name != f.name: print("Found non-normalised file!", f)
