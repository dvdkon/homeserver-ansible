DATA_DIR_ROOT = "/mnt/data/userdata"
LDAP_SERVER_URL = "ldap://10.1.0.25"
LDAP_BIND_DN = "uid=ldap-svc-datadirman,ou=users,dc=konarici,dc=cz"
LDAP_BASE = "dc=konarici,dc=cz"
PASSWORDS_INI = "secrets/passwords.ini"

from configparser import ConfigParser
passwords_ini = ConfigParser()
passwords_ini.read(PASSWORDS_INI)
LDAP_BIND_PW = passwords_ini["ldap_sw"]["datadirman"]

# Groups where each user has write permissions
GROUPS_RW = {"konarici"}
# Groups where only the owner has write permissions
GROUPS_R = {"pribuzni"}
# Other groups are ignored

ROOT_USER = "ldaproot"
ANONYMOUS_USER = "anonymous"
ANONYMOUS_WRITE_GROUP = "anonymous-rw"
